package frontend;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

class Config {
	private Integer	difficulty;
	private Integer gamesizeX;
	private Integer gamesizeY;
	private File config;

	Config() throws IOException {
		
		if(System.getProperty("os.name").contains("Windows")) {
			config=new File(System.getProperty("user.home")+"/AppData/Local/Frogger/config.txt");
		}else {
			config=new File(System.getProperty("user.home")+"/.config/Frogger/config.txt");
		}
		if(!config.exists()) {
			if(config.getParentFile().mkdirs())
				writeDefaultConfig();
			else
				throw new IOException();
		}else {
			loadConfig();
		}
	}
	private void createFile() throws IOException {
        config.createNewFile();
	}
	void writeDefaultConfig() throws IOException {
		difficulty=10;
		gamesizeX=15;
		gamesizeY=10;
		writeConfig();
	}
	private void writeConfig() throws IOException {
		config.delete();
		createFile();
		BufferedWriter out = new BufferedWriter(new FileWriter(config));
		out.write(difficulty.toString());
		out.newLine();
		out.write(gamesizeX.toString());
		out.newLine();
		out.write(gamesizeY.toString());
		out.close();
	}
	void loadConfig() throws NumberFormatException, IOException {
		BufferedReader in = new BufferedReader(new FileReader(config));
		difficulty=Integer.parseInt(in.readLine());
		gamesizeX=Integer.parseInt(in.readLine());
		gamesizeY=Integer.parseInt(in.readLine());
		in.close();
	}

	int getGamesizeX() {
		return gamesizeX;
	}

	int getGamesizeY() {
		return gamesizeY;
	}

	int getDifficulty() {
		return difficulty;
	}
	private void setDifficulty(Integer difficulty) {
		difficulty=difficulty<1||difficulty>100?10:difficulty;
		this.difficulty = difficulty;
	}
	private void setGamesizeX(Integer gamesizeX) {
		gamesizeX=gamesizeX<5||gamesizeX>20?15:gamesizeX;
		this.gamesizeX = gamesizeX;
	}
	private void setGamesizeY(Integer gamesizeY) {
		gamesizeY=gamesizeY<10||gamesizeY>20?10:gamesizeY;
		this.gamesizeY = gamesizeY;
	}
	void setConfig(int[] a) throws IOException {
		setDifficulty(a[0]);
		setGamesizeX(a[1]);
		setGamesizeY(a[2]);
		writeConfig();
	}
}

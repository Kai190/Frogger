package frontend;

import java.io.IOException;

import controller.GameController;
import displays.Console;
import displays.DisplayAble;
import resources.Direction;

public class MainController {
	
	GameController con;
	Config config;
	DisplayAble dis;
	Menu menu;
	ErrorMessages error;
	
	MainController(){
		error= new ErrorMessages();
		dis = new Console();
		try {
			config = new Config();
		} catch (IOException e) {
			dis.printError(error.getError(0));
			System.exit(1);
		}
		menu=new Menu(dis);
		boolean start=false;
		while(!start) {
			Input mm=null;;
			do {
				try {
					mm=menu.mainMenu();
				} catch (IOException e) {
					dis.printError(error.getError(3));
					System.exit(1);
				}
			}while(mm==null);
			switch(mm) {
			case CONFIG:
				boolean cont=false;
				while(!cont) {
					Input cm=null;
					do {
						try {
							cm=menu.configMenu();
						} catch (IOException e) {
							dis.printError(error.getError(3));
							System.exit(1);
						}
					}while(cm==null);
					switch(cm) {
					case CONFIG:
						dis.printText("Default?(y/n)");
						try {
							if(Character.toLowerCase(dis.getMenuInput())=='y') {
								config.writeDefaultConfig();
							}
							else {
								try {
									config.setConfig(dis.getConfigInput());
								} catch (NumberFormatException e) {
									dis.printError(error.getError(2));
									config.writeDefaultConfig();
								}
							}
						} catch (IOException e) {
							dis.printError(error.getError(0));
							System.exit(1);
						}
						break;
					case CONTINUE:
						cont=true;
						break;
					case LOAD:
						try {
							config.loadConfig();
						} catch (NumberFormatException e) {
							dis.printError(error.getError(2));
							try {
								config.writeDefaultConfig();
							} catch (IOException e1) {
								dis.printError(error.getError(0));
								System.exit(1);
							}
						} catch (IOException e) {
							dis.printError(error.getError(1));
							try {
								config.writeDefaultConfig();
							} catch (IOException e1) {
								dis.printError(error.getError(0));
								System.exit(1);
							}
						}
						break;
					default:
						throw new RuntimeException(error.getError(4));
					}
					break;
				}
				break;
			case END:
				dis.printText("Bye Bye!");
				System.exit(0);
			case START:
				start=true;
				break;
			default:
				throw new RuntimeException(error.getError(4));
			}
		}
		con = new GameController(config.getGamesizeX(),config.getGamesizeY());
		dis.printMatrix(con.getMatrix());
		run(config.getDifficulty());
	}
	
	private void run(int difficulty) {
		boolean run = true;
		int countfortick=0;
		while(run) {
			countfortick++;
			try {
				Thread.sleep(34);
			} catch (InterruptedException e) {
				e.printStackTrace();
				break;
			}
			Input c=null;
			try {
				c = dis.getInput();
			} catch (IOException e1) {
				dis.printError(error.getError(3));
				System.exit(1);
			}
			if(c != null) {
				switch(c) {
					case DOWN:
						con.moveFrog(Direction.DOWN);
						break;
					case LEFT:
						con.moveFrog(Direction.LEFT);
						break;
					case PAUSE:
						boolean cont=false;
						while(!cont) {
							Input pm=null;
							do {
								try {
									pm=menu.pauseMenu();
								} catch (IOException e) {
									dis.printError(error.getError(3));
									System.exit(1);
								}
							}while(pm==null);
							switch(pm) {
							case CONTINUE:
								cont=true;
								break;
							case END:
								dis.printText("Bye Bye!");
								System.exit(0);
								break;
							case RESTART:
								return;
							default:
								throw new RuntimeException(error.getError(4));
							}
						}
						break;
					case RIGHT:
						con.moveFrog(Direction.RIGHT);
						break;
					case UP:
						con.moveFrog(Direction.UP);
						break;
					default:
						throw new RuntimeException(error.getError(4));
				}
				dis.printMatrix(con.getMatrix());
			}
			if(countfortick==difficulty) {
				countfortick=0;
				con.tick();
				dis.printMatrix(con.getMatrix());
			}
			if(con.hit()) {
				System.out.println("\nTot");
				System.out.println(con.getScore());
				break;
			}
			if(con.isWon()) {
				System.out.println("Sieg!");
				break;
			}
		}
	}
	public static void main(String[] args) {
		while(true) {
			new MainController();
		}
	}
}
package displays;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import controller.GameController;
import frontend.Input;
import resources.Direction;
import resources.GameObject;
import resources.LogicalField;
import resources.MovableObject;

public class Console implements DisplayAble{
	
	BufferedReader in;
	
	public Console() {
		in=new BufferedReader(new InputStreamReader(System.in));
	}
	
	
	@Override
	public void printMatrix(ArrayList<ArrayList<ArrayList<LogicalField>>> matrix) {
		clear();
		for(int iy=0;iy<GameController.Matrixsizey;iy++) {
			for(int ix=0;ix<GameController.Matrixsizex;ix++) {
				for(int iz=GameController.LayerNum-1;iz>=0;iz--) {
					GameObject go =matrix.get(iy).get(ix).get(iz).getGo();
					if(go != null) {
						printObject(go);
						break;
					}
				}
			}
			System.out.println();
		}
	}
	
	private void printObject(GameObject go) {
		switch(go.getName()) {
		case TRUCK:
		case CAR:
			if(((MovableObject)go).getDi()==Direction.RIGHT)
				System.out.print(">");
			else
				System.out.print("<");
			break;
		case FROG:
			System.out.print("^");
			break;
		case GRASS:
			System.out.print("'");
			break;
		case LOG:
			System.out.print("#");
			break;
		case STREET:
			System.out.print("_");
			break;
		case TURTLE:
			System.out.print("O");
			break;
		case WATER:
			System.out.print("~");
			break;
		case GOAL:
			System.out.print("X");
			break;
		}
	}

	@Override
	public Input getInput() throws IOException {
		if(in.ready()) {
			char tmp=(char) Character.toLowerCase((in.read()));
			while(in.ready())
				in.read();
			switch(tmp) {							//TODO Make button config
				case 'w':
					return Input.UP;
				case 'a':
					return Input.LEFT;
				case 's':
					return Input.DOWN;
				case 'd':
					return Input.RIGHT;
				case 'p':
					return Input.PAUSE;
				}
		}
		return null;
	}


	@Override
	public void printText(String str) {
		System.out.println(str);
	}


	@Override
	public void clear() {
		System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
	}


	@Override
	public int[] getConfigInput() throws NumberFormatException, IOException {
		System.out.println("Please insert the Difficulty [1 (Hard) to 100 (Easy)], The Gamesize for X and Y");
		int[] a =new int[3];
		a[0]=Integer.parseInt(in.readLine());
		a[1]=Integer.parseInt(in.readLine());
		a[2]=Integer.parseInt(in.readLine());
		return a;
	}


	@Override
	public Character getMenuInput() throws IOException {
		String input= in.readLine();
		if(input!="")
			return  input.charAt(0);
		return null;
	}


	@Override
	public void printError(String string) {
		clear();
		System.err.println(string);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {}
	}
}

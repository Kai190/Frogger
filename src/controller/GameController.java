package controller;

import java.util.ArrayList;

import resources.*;

public class GameController {
	
	private ArrayList<ArrayList<ArrayList<LogicalField>>> matrix;
	public static int Matrixsizex,Matrixsizey,LayerNum;
	private Frog frog;
	private ArrayList<Goal> goals;
	
	public GameController(int x,int y) {
		Matrixsizex=x;
		Matrixsizey=y;
		LayerNum=3;
		this.initMatrix();
		this.standardGame();
		this.executeMatrix();
	}
	
	public ArrayList<ArrayList<ArrayList<LogicalField>>> getMatrix() {
		return matrix;
	}

	public void moveFrog(Direction di) {
		matrix.get(frog.getPosy()).get(frog.getPosx()).get(LayerNames.FROG.getNumVal()).remove();
		frog.move(di);
		matrix.get(frog.getPosy()).get(frog.getPosx()).get(LayerNames.FROG.getNumVal()).setGo(frog);
	}
	
	public boolean isWon() {
		for(int i=0;i<goals.size();i++) {
			if(!goals.get(i).isReached())
				return false;
		}
		return true;
	}

	public boolean hit() {
		for(int i=LayerNum-1;i>=0;i--) {
			GameObject go = matrix.get(frog.getPosy()).get(frog.getPosx()).get(i).getGo();
			if(go!=null&&!(go instanceof Frog)) {
				if(go instanceof Goal) {
					frog=new Frog(Matrixsizex/2, Matrixsizey-1);
					matrix.get(frog.getPosy()).get(frog.getPosx()).get(LayerNames.FROG.getNumVal()).add(frog);
				}
				if(((Hitable)go).onHit()) {
					return true;
				}
				break;
			}
		}
		return false;
	}

	public void tick() {
		for(int iy=0;iy<Matrixsizey;iy++) {
			for(int ix=0;ix<Matrixsizex;ix++) {
				for(int iz=0;iz<LayerNum;iz++) {
					GameObject go =matrix.get(iy).get(ix).get(iz).getGo();
					if(go instanceof MovableObject&&!((MovableObject)go).isMoved()) {
						GameObject maybeFrog=matrix.get(iy).get(ix).get(iz+1).getGo();
						if(maybeFrog instanceof Frog&&!((Frog)maybeFrog).isDragged()) {
							moveFrog(((MovableObject)go).getDi());
							((Frog)maybeFrog).setDragged(true);
						}
						matrix.get(iy).get(ix).get(LayerNames.MOVING.getNumVal()).remove();
						((MovableObject)go).move(Direction.CURRENT);
						((MovableObject)go).setMoved(true);
						matrix.get(go.getPosy()).get(go.getPosx()).get(LayerNames.MOVING.getNumVal()).add(go);
					}
				}
			}
		}
		for(int iy=0;iy<Matrixsizey;iy++) {
			for(int ix=0;ix<Matrixsizex;ix++) {
				for(int iz=0;iz<LayerNum;iz++) {
					matrix.get(iy).get(ix).get(iz).execute();
				}
			}
		}
		for(int iy=0;iy<Matrixsizey;iy++) {
			for(int ix=0;ix<Matrixsizex;ix++) {
				for(int iz=0;iz<LayerNum;iz++) {
					GameObject go =matrix.get(iy).get(ix).get(iz).getGo();
					if(go instanceof MovableObject) {
						((MovableObject)go).setMoved(false);
					}
					if(go instanceof Frog) {
						((Frog)go).setDragged(false);
					}
				}
			}
		}
	}
	
	private void executeMatrix() {
		for(int iy=0;iy<Matrixsizey;iy++) {
			for(int ix=0;ix<Matrixsizex;ix++) {
				for(int iz=0;iz<LayerNum;iz++) {
					matrix.get(iy).get(ix).get(iz).execute();
				}
			}
		}
	}
	
	private void initMatrix() {													//2 dimensional field with 3rd Dimension for multiple objects on same field
		matrix = new ArrayList<ArrayList<ArrayList<LogicalField>>>();			//Layer [0] = Ground
		for(int iy=0;iy<Matrixsizey;iy++) {										//Layer [1] = Movable
			matrix.add(new ArrayList<ArrayList<LogicalField>>());				//Layer [2] = Frog/Item
			for(int ix=0;ix<Matrixsizex;ix++) {
				matrix.get(iy).add(new ArrayList<LogicalField>());
				for(int iz=0;iz<LayerNum;iz++) {
					matrix.get(iy).get(ix).add(new LogicalField());
				}
			}
		}
		goals=new ArrayList<Goal>();
	}
	private void standardGame() { // TODO Fix Scaling
		for(int ix=0;ix<Matrixsizex;ix++) {
			if(ix%2==0) {
				matrix.get(0).get(ix).get(LayerNames.GROUND.getNumVal()).add(new Ground(ix,0,ObjectNames.GRASS));	
			}
			else {
				goals.add(new Goal(ix,0));
				matrix.get(0).get(ix).get(LayerNames.GROUND.getNumVal()).add(goals.get(ix/2));
			}
		}
		
		for(int iy=1;iy<=Matrixsizey/2-2;iy++) {
			for(int ix=0;ix<Matrixsizex;ix++) {
				matrix.get(iy).get(ix).get(LayerNames.GROUND.getNumVal()).add(new Water(ix,iy));
			}
		}
		for(int ix=0;ix<Matrixsizex;ix++) {
			matrix.get(Matrixsizey/2-1).get(ix).get(LayerNames.GROUND.getNumVal()).add(new Ground(ix,Matrixsizey/2+1, ObjectNames.GRASS));
		}
		for(int iy=Matrixsizey/2;iy<=Matrixsizey-2;iy++) {
			for(int ix=0;ix<Matrixsizex;ix++) {
				matrix.get(iy).get(ix).get(LayerNames.GROUND.getNumVal()).add(new Ground(ix,iy,ObjectNames.STREET));
			}
		}
		for(int ix=0;ix<Matrixsizex;ix++) {
			matrix.get(Matrixsizey-1).get(ix).get(LayerNames.GROUND.getNumVal()).add(new Ground(ix,Matrixsizey-1,ObjectNames.GRASS));
		}
		matrix.get(5).get(5).get(LayerNames.MOVING.getNumVal()).add(new Car(5,5, ObjectNames.CAR, Direction.LEFT, 0.0));
		matrix.get(7).get(7).get(LayerNames.MOVING.getNumVal()).add(new Car(7,7, ObjectNames.CAR, Direction.RIGHT, 0.0));
		matrix.get(1).get(1).get(LayerNames.MOVING.getNumVal()).add(new Floating(1,1, ObjectNames.LOG, Direction.LEFT, 0.0));
		matrix.get(1).get(2).get(LayerNames.MOVING.getNumVal()).add(new Floating(2,1, ObjectNames.LOG, Direction.LEFT, 0.0));
		matrix.get(2).get(3).get(LayerNames.MOVING.getNumVal()).add(new Floating(3,2, ObjectNames.LOG, Direction.RIGHT, 0.0));
		matrix.get(2).get(4).get(LayerNames.MOVING.getNumVal()).add(new Floating(4,2, ObjectNames.LOG, Direction.RIGHT, 0.0));
		matrix.get(3).get(5).get(LayerNames.MOVING.getNumVal()).add(new Floating(5,3, ObjectNames.LOG, Direction.LEFT, 0.0));
		matrix.get(3).get(6).get(LayerNames.MOVING.getNumVal()).add(new Floating(6,3, ObjectNames.LOG, Direction.LEFT, 0.0));
		
		frog = new Frog(Matrixsizex/2, Matrixsizey-1);
		matrix.get(frog.getPosy()).get(frog.getPosx()).get(LayerNames.FROG.getNumVal()).add(frog);
	}

	public int getScore() {
		int count=0;
		for(int i=0;i<goals.size();i++) {
			if(goals.get(i).isReached())
				count++;
		}
		return count;
	}
}

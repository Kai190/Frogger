package resources;

import controller.GameController;

public abstract class MovableObject extends GameObject {

	private Direction di;
	private double velo;
	private boolean moved;
	
	public MovableObject(int posx, int posy, ObjectNames name, Direction di, double velo) {
		super(posx,posy,name);
		this.di = di;
		this.velo=velo;
		this.moved=false;
	}
	
	public void move(Direction r) {//TODO Velocity
		if(r!=Direction.CURRENT)
			this.di = r;
		switch(di) {
		case DOWN:
			this.setPosy(this.getPosy()+1);
			break;
		case LEFT:
			this.setPosx(this.getPosx()-1);
			break;
		case RIGHT:
			this.setPosx(this.getPosx()+1);
			break;
		case UP:
			this.setPosy(this.getPosy()-1);
			break;
		default:
			break;
		}
		if(this.getPosy()<0) {
			this.setPosy(GameController.Matrixsizey-1);
		}
		else if(this.getPosy()==GameController.Matrixsizey) {
			this.setPosy(0);
		}
		else if(this.getPosx()<0) {	
			this.setPosx(GameController.Matrixsizex-1);
		}
		else if(this.getPosx()==GameController.Matrixsizex) {
			this.setPosx(0);
		}
	}

	public Direction getDi() {
		return di;
	}

	public double getVelo() {
		return velo;
	}

	public boolean isMoved() {
		return moved;
	}

	public void setMoved(boolean moved) {
		this.moved = moved;
	}
	
}

package resources;

public interface Hitable {
	public boolean onHit();
}

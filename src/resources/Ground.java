package resources;

public class Ground extends GameObject implements Hitable{

	public Ground(int posx, int posy, ObjectNames name) {
		super(posx, posy, name);
	}

	@Override
	public boolean onHit() {
		return false;
	}

}

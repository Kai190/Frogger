package resources;

public class Floating extends MovableObject implements Hitable {

	public Floating(int posx, int posy, ObjectNames name, Direction di, double velo) {
		super(posx, posy, name, di, velo);
	}

	@Override
	public boolean onHit() {
		return false;
	}

}

package resources;

public class Water extends GameObject implements Hitable{

	public Water(int posx, int posy) {
		super(posx, posy, ObjectNames.WATER);
	}

	@Override
	public boolean onHit() {
		return true;
	}

}

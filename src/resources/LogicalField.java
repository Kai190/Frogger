package resources;

import java.util.ArrayList;

public class LogicalField {
	
	private GameObject go;
	private ArrayList<GameObject> waitingLine;
	
	public LogicalField() {
		go=null;
		waitingLine=new ArrayList<GameObject>();
		waitingLine.add(null);
	}
	
	public GameObject getGo() {
		return go;
	}
	
	public void add(GameObject go) {
		waitingLine.add(go);
	}
	
	public void remove() {
		go=null;
		waitingLine.set(0, null);
	}
	
	public void execute() {
		go=waitingLine.get(waitingLine.size()-1);
		waitingLine.clear();
		waitingLine.add(go);
	}

	public void setGo(Frog frog) {
		go=frog;
		waitingLine.clear();
		waitingLine.add(go);
	}
}

package resources;

public abstract class GameObject {

	private int posx;
	private int posy;
	private ObjectNames name;

	public GameObject(int posx, int posy, ObjectNames name) {
		this.posx=posx;
		this.posy=posy;
		this.name=name;
	}
	
	public ObjectNames getName() {
		return name;
	}

	public int getPosx() {
		return posx;
	}

	public int getPosy() {
		return posy;
	}

	protected void setPosx(int posx) {
		this.posx = posx;
	}

	protected void setPosy(int posy) {
		this.posy = posy;
	}
}
package resources;

public class Car extends MovableObject implements Hitable {

	public Car(int posx, int posy, ObjectNames name, Direction di, double velo) {
		super(posx, posy, name, di, velo);
	}

	@Override
	public boolean onHit() {
		return true;
	}

}
